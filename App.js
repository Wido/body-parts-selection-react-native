import React from 'react'
import {View, Text,StyleSheet,Button} from 'react-native'
import {HumanBody} from './humanBody'
import Svg, {G, Path} from "react-native-svg";

class App extends React.Component {
  state = {
    togglePressHead: false,
    togglePressNeck: false,
    togglePressChest: false,
    togglePressRightArm: false,
    togglePressLeftArm: false,    
  };
      render() {
        return (
          <View style={styles.sectionContainer}>
            <Text style={styles.appTitle}>Spinna Bifida</Text>
            <View>
              <Text style={styles.sectionTitle}>Where is your pain?</Text>
            </View>    

                  <Svg  height="280.63" width="198.425" viewBox="0 0 595.275590551 841.88976378" >
                    <G fill-rule="evenodd" clip-rule="evenodd">
                        <Path id="head"  style="stroke-width:0.2835;stroke:#2d2c2b;fill-rule:evenodd;stroke-miterlimit:10.433;" d="M 214.9581,76.3486 C 214.9581,108.0618 218.9346,151.1059 218.7311,170.9292 233.6169,188.4038 248.3603,207.5716 263.3885,223.3531 277.6271,224.6475 293.7685,222.183 308.0071,223.4774 320.3041,209.886 330.703,198.8948 342.9999,185.3034 349.3623,155.8777 354.9293,131.2236 352.7034,73.8478 354.9293,41.3558 265.0614,-20.6769 214.9581,76.3486 Z" 
                                onPress={() =>this.setState({
                                  togglePressHead: !this.state.togglePressHead,                                  
                                })
                          } fill={this.state.togglePressHead ? '#d9d': '#d9dbdc' } />
                        <Path id="neck" style="stroke-width:0.2835;stroke:#2d2c2b;fill-rule:evenodd;stroke-miterlimit:10.433;" d="M 238.0216,213.1386 C 238.0216,229.0444 238.0216,244.9502 238.0216,260.856 236.431,264.0372 234.8404,267.2184 233.2498,270.3995 244.1188,276.4968 254.9878,282.594 265.8567,288.6912 281.4974,288.6912 297.1382,288.6912 312.7789,288.6912 324.1781,279.6779 335.5772,270.6646 346.9764,261.6513 343.7952,256.6145 340.6141,251.5776 337.4329,246.5408 337.1678,233.0209 336.9027,219.5009 336.6376,205.981 328.1545,213.4037 319.6714,220.8264 311.1883,228.2491 295.2825,229.3095 279.3767,230.3699 263.4709,231.4303 255.2529,226.6585 247.0348,221.8868 238.0216,213.1386 Z" 
                               onPress={() =>this.setState({
                                togglePressNeck: !this.state.togglePressNeck,                                  
                              })
                              } 
                        fill={this.state.togglePressNeck ? '#d9d': '#d9dbdc' } />
                        <Path id="chest"fill={this.state.togglePressChest ? '#d9d': '#d9dbdc'} style="stroke-width:0.2835;stroke:#2d2c2b;fill-rule:evenodd;stroke-miterlimit:10.433;" d="M 225.2969,273.5807 L 263.4709,296.6441 313.5742,297.4394 350.9528,271.1948 374.0163,292.6677 402.6467,365.0391 415.3714,561.4759 333.4564,544.7748 204.6193,550.3419 149.7443,567.043 154.516,373.7873 191.8947,293.463 208.5958,283.1242 225.2969,273.5807 Z" 
                                onPress={() =>this.setState({
                                  togglePressChest: !this.state.togglePressChest,                                  
                                })
                                }/>
                        <Path id="left-arm" class="path_shape" fill={this.state.togglePressLeftArm ? '#d9d':'#d9dbdc'} style="stroke-width:0.2835;stroke:#2d2c2b;fill-rule:evenodd;stroke-miterlimit:10.433;" d="M 179.17,298.2347 L 111.5703,330.0463 82.9399,397.646 66.2388,498.648 58.2858,531.2549 71.0105,562.2712 106.0033,569.4289 126.6808,549.5466 146.5631,488.3092 151.3349,366.6297 179.17,298.2347 Z" 
                                onPress={() =>this.setState({
                                  togglePressLeftArm: !this.state.togglePressLeftArm,                                  
                                })
                                }
                                />
                        <Path id="righ-arm" fill={this.state.togglePressRightArm ? '#d9d':'#d9dbdc'} style="stroke-width:0.2835;stroke:#2d2c2b;fill-rule:evenodd;stroke-miterlimit:10.433;" d="M 384.3516,292.5348 L 447.061,324.3465 473.6203,391.9462 489.1132,492.9481 496.4908,525.555 484.6866,556.5714 452.2253,563.729 433.0436,543.8467 414.5997,482.6093 410.1731,360.9299 384.3516,292.5348 Z" 
                                onPress={() =>this.setState({
                                  togglePressRightArm: !this.state.togglePressRightArm,                                  
                                })
                                }
                                />
                                          
                    </G>
                  </Svg>            
            <Button title=" NEXT "/>
          </View>    
        );
    }
}

export default App;


const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 40,                 
    alignItems: "center",
  },
   
  appTitle:{
    fontSize:36,
    fontWeight: '900',
    color:'#00A0AF',
    fontWeight: 'bold',    
    textAlign: 'center',
  },
  sectionTitle: {
    fontSize: 24,
    paddingTop: 30,
    fontWeight: 'bold',      
    color: '#000',      
    textAlign: 'center',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    textAlign: 'right',
    color: '#f0f',
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: '#f0f',
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});


